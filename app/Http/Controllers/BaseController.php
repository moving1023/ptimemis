<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Response;

class BaseController extends Controller
{
	public function jsonResponse($error,$data){
		return response()->json(["error"=>$error,"result"=>$data])->header('Status', 200);
	}

	public function tableConfig($table){
		return require "../storage/app/config/$table.php";
	}
}