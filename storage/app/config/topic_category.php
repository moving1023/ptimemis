<?php return array (
  'base' => 
  array (
    'name' => 'topic_category',
    'comment' => '',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
      'is_hide' => 0,
    ),
    1 => 
    array (
      'name' => 'topic_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '专题页id',
      'is_hide' => 0,
      'relate'=> [
        'table' =>'topic',
        'field' =>'id',
        'select'=>'title',
        'filter' =>"",
        'name'  =>'topic_title',
        'comment' => '专题页'
      ]
    ),
    2 => 
    array (
      'name' => 'category_module',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '关联分类的模型',
      'is_hide' => 0,
    ),
    3 => 
    array (
      'name' => 'category_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '关联分类的id',
      'is_hide' => 0,
      'relate'=> [
        'table' =>'category',
        'field' =>'id',
        'select'=>'name',
        'filter' =>"",
        'name'  =>'category_name',
        'comment' => '关联分类'
      ]
    ),
    4 => 
    array (
      'name' => 'config',
      'type' => 'varchar(200)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => 'json配置',
      'is_hide' => 0,
    ),
    5 => 
    array (
      'name' => 'weight',
      'type' => 'smallint(6)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '排序权重',
      'is_hide' => 0,
    ),
  ),
);