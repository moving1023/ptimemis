<?php return array (
  'base' => 
  array (
    'name' => 'alias',
    'comment' => '别名表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
      'is_hide' => 0,
    ),
    1 => 
    array (
      'name' => 'object',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '实体',
      'is_hide' => 0,
    ),
    2 => 
    array (
      'name' => 'object_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '实体id',
      'is_hide' => 0,
    ),
    3 => 
    array (
      'name' => 'name',
      'type' => 'varchar(200)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '名称',
      'is_hide' => 0,
    ),
  ),
);