<?php return array (
  'base' => 
  array (
    'name' => 'tag',
    'comment' => '标签表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
      'is_hide' => 0,
    ),
    1 => 
    array (
      'name' => 'name',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '名称',
      'is_hide' => 0,
    ),
    2 => 
    array (
      'name' => 'created_at',
      'type' => 'datetime',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '创建时间',
      'is_hide' => 0,
    ),
    3 => 
    array (
      'name' => 'created_by',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '创建用户id',
      'is_hide' => 0,
    ),
  ),
);