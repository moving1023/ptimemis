<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="myApp" class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>天道棋院</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="favicon.ico" />

	<link rel="stylesheet" href="./bower_components/lumx/dist/lumx.css">
    <link rel="stylesheet" href="./css/app.css">

</head>
<body >
<div ng-controller="GlobalController" flex-container="row"  class="bgc-grey-50" style="height:100%">
    <div flex-item="2" class="bgc-blue-grey-900 tc-white-2 ">
    	<div class="toolbar bgc-blue-grey-800 tc-white-1" style="height:48px;padding-top:4px;padding-bottom:4px;">
    		<span class="toolbar__label fs-title">天道棋院</span>
    	</div>
    	<div ng-repeat="menuOne in menuData" >
	    	<span class="fs-title display-block mb p+" ng-bind="menuOne.name"></span>
	        <div class="divider divider--dark"></div>
	        <ul class="list mt+">
	            <li class="list-row list-row--has-separator" ng-class="{active:menuTwo == breadcrumbData[1]}" ng-click="stateGo(menuTwo)" ng-repeat="menuTwo in menuOne.son">
	            	<div class="list-row__primary">
	                    <i class="icon icon--s icon--grey icon--flat mdi mdi-{{menuTwo.ico}}"></i>
	                </div>
	                <div class="list-row__content">
	                    <span ng-bind="menuTwo.name"></span>
	                </div>
	            </li>
	        </ul>
	    </div>
    </div>
    <div flex-item="10">
	    <div class="toolbar bgc-teal-A700 " style="height:48px;padding-top:4px;padding-bottom:4px;">
		    <div class="toolbar__left mr+++">
		        <button class="btn btn--l btn--black btn--icon" lx-ripple>
		            <i class="mdi mdi-menu"></i>
		        </button>
		    </div>
		    <span class="toolbar__label">timepicker.cn</span>
		    <div class="toolbar__right">
		        <lx-search-filter closed theme="light"></lx-search-filter>

		        <lx-dropdown position="right" from-top>
		            <button class="btn btn--l btn--black btn--icon" lx-ripple lx-dropdown-toggle>
		                <i class="mdi mdi-dots-vertical"></i>
		            </button>

		            <lx-dropdown-menu>
		                <ul>
		                    <li><a class="dropdown-link">个人中心</a></li>
		                    <li class="dropdown-divider"></li>
		                    <li><a class="dropdown-link dropdown-link--is-header">其他</a></li>
		                    <li><a class="dropdown-link">登出</a></li>
		                </ul>
		            </lx-dropdown-menu>
		        </lx-dropdown>
		    </div>
		</div>
		<div class="p+">
		<div class="mb breadcrumbs">
			<span><i class="mdi mdi-home tc-grey-500"></i></span>
			<span class="level" ng-repeat="breadcrumb in breadcrumbData" ng-bind="breadcrumb.name"></span>
		</div>
		<div class="data-table-container card">
			
			<span class="p++ bgc-blue-grey-50 fs-caption display-block" ng-bind="breadcrumbData[1]['description']"></span>

		    <lx-tabs indicator="green-200" active-tab="0" layout="inline" ng-if="breadcrumbData[1]['son']" >
		        <lx-tab heading="{{tab.name}}"  ng-repeat="tab in breadcrumbData[1]['son']">
		            <p class="fs-caption p+" ng-bind="tab.description"><a style="cursor:pointer;" ng-click="stateGo(tab)" >Go</a></p>
		        </lx-tab>
		    </lx-tabs>
			
			<div ui-view></div>


		</div>   	
		</div>
    </div>
</div>


<script src="./bower_components/jquery/dist/jquery.min.js"></script>
<script src="./bower_components/velocity/velocity.min.js"></script>
<script src="./bower_components/moment/min/moment-with-locales.min.js"></script>
<script src="./bower_components/angular/angular.min.js"></script>
<script src="./bower_components/lumx/dist/lumx.min.js"></script>

<script src="./bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
<script src="./bower_components/angular-resource/angular-resource.min.js"></script>
<script src="./views/object/ObjectController.js"></script>

<script src="./app.js"></script>

</body>
</html>

